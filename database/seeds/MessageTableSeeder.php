<?php

use Illuminate\Database\Seeder;

class MessageTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'slug' => 'message.message.view',
                'name' => 'View Message',
            ],
            [
                'slug' => 'message.message.create',
                'name' => 'Create Message',
            ],
            [
                'slug' => 'message.message.edit',
                'name' => 'Update Message',
            ],
            [
                'slug' => 'message.message.delete',
                'name' => 'Delete Message',
            ],
        ]);
    }
}
