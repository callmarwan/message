<?php

return [

    /**
     * Provider.
     */
    'provider' => 'souktel',

    /*
     * Package.
     */
    'package'  => 'message',

    /*
     * Modules.
     */
    'modules'  => ['message'],

    'message'  => [
        'model'         => 'SoukTel\Message\Models\Message',
        'table'         => 'messages',
        'presenter'     => \SoukTel\Message\Repositories\Presenter\MessageItemPresenter::class,
        'hidden'        => [],
        'visible'       => [],
        'guarded'       => ['*'],
        'slugs'         => ['slug' => 'name'],
        'dates'         => ['deleted_at'],
        'appends'       => [],
        'fillable'      => ['user_id', 'user_type', 'status', 'star', 'from', 'to', 'subject', 'message', 'read', 'type', 'upload_folder'],
        'translate'     => [],

        'upload_folder' => '/message/message',
        'uploads'       => [
            'single'   => [],
            'multiple' => [],
        ],
        'casts'         => [
        ],
        'revision'      => [],
        'perPage'       => '20',
        'search'        => [
            'name' => 'like',
            'status',
        ],
    ],
];
