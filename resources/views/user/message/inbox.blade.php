<div class="col-sm-4 box-message-body-user">
    <div class="box-message-search">
        <div class="input-group">
            <input id="message-search" name="message" placeholder="Search" class="form-control" type="text">
            <span class="input-group-btn">
              <button type="button" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>

	@include('message::user.message.inboxChild')
</div>
<div class="visible-xs" id="message-search-break">

</div>