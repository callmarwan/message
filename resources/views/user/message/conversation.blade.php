<!-- Conversations are loaded here -->
<div id="conversation">
    <div class="direct-chat-messages">
            @if(!empty($messages))
                @foreach($messages as $message)
                    <div id="message-{{$message->id}}" class="direct-chat-msg {{$message->user->id == auth()->user()->id ? 'right' : '' }}">
                        <img class="direct-chat-img" src="{{ $message->user->img ? url($message->user->img) : "" }}" alt="message user image" data-toggle="tooltip" data-placement="{{$message->user->id == auth()->user()->id ? 'left' : 'right' }}" title=""
                             data-original-title="{{$message->user->id == auth()->user()->id ?
                             'You, '.\Carbon\Carbon::parse($message->created_at)->format('F jS, Y h:ia') :
                             $message->user->username.', '.\Carbon\Carbon::parse($message->created_at)->format('F jS, Y h:ia') }}">
                        <div class="direct-chat-text">
                            <a href="#" class="talkDeleteMessage" data-message-id="{{$message->id}}" title="Delete Message"><i class="fa fa-close"></i></a>

                            {{$message->message}}
                        </div>
                        {{--<span class="direct-chat-timestamp pull-left"> <i class="fa fa-check"></i> Seen 23 Jan 2:00 pm</span>--}}
                        {{--<span class="direct-chat-timestamp">Delete</span>--}}
                    </div>
                @endforeach
            @else
            <div class="direct-chat-msg text-center ">
                {{ trans("Message::message.start") }} <span class="chatUserName">SomeOne</span>
                {{--<span class="direct-chat-timestamp pull-left"> <i class="fa fa-check"></i> Seen 23 Jan 2:00 pm</span>--}}
                {{--<span class="direct-chat-timestamp">Delete</span>--}}
            </div>
            @endif

            <!-- Message to the right start -->
            <div class="direct-chat-msg right">
                <div class="direct-chat-info clearfix">
                    {{--@if($message->is_seen)--}}
                        {{--<span class="direct-chat-timestamp pull-left"> <i class="fa fa-check"></i> Seen 23 Jan 2:00 pm</span>--}}
                    {{--@endif--}}
                </div>
            </div>
            <!-- Message to the right end -->

    </div>
</div>

