<div class="row">
    <div class="col-sm-12">
        <div class="dashboard-content">
            <div class="box-body box-message-body">
                @include('message::user.message.inbox')
                @include('message::user.message.message')
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        var width = $('html').width();
//            alert(width);
        if(width > 768) {
            $('.message-user').slimScroll({
                height: '485px'
            });
        }

        $('.direct-chat-messages').slimScroll({
            height: '400x',
            start: 'bottom'
        });

        $("#select2").select2({

        });
//            $(".select2-results__option").prop("aria-selected", false);
        $("#select2-select2-container").prop("title", "");
        $("#select2-select2-container").html("");
        $(".select2-container--default .select2-selection--single .select2-selection__arrow").html("");
        $(".select2.select2-container.select2-container--default").hide();

    });

    $('body').on('click', '.talkDeleteMessage', function (e) {
        e.preventDefault();
        var tag, url, id, request;

        tag = $(this);
        MessageId = tag.data('message-id');

        if(!confirm('Do you want to delete this message?')) {
            return false;
        }

        request = $.ajax({
            method: "post",
            url: '{{ route('delete.message') }}',
            data: {MessageId : MessageId},

        });



        request.done(function(response) {
            if (response.status == 'success') {
                $('#message-' + MessageId).hide(500, function () {
                    $(this).remove();
                });
            }
        });
    })

    $("#message-search").keyup(function () {
        var filter = $(this).val();
        $(".message-user li").each(function () {

            var width = $('html').width();
            if(width < 768) {

                if(filter.length > 0 ) {
                    $(".message-user").removeClass('hidden-xs').show();
                    $('#message-search-break').html('<br>');
                } else {
                    $(".message-user").addClass('hidden-xs').hide();
                    $('#message-search-break').html('');
                }
            }

            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });

    $("#delete-message").click(function () {
        var filter = jQuery(this).val();
        $(".direct-chat-messages .direct-chat-msg").each(function () {
            if($(this).length) {

                $(this).prepend( "<span style='float:left;margin:10px 5px 0px 0px;'><input type='checkbox'></span>" );
            }

            var classSplit = $(this).attr('class').split(' ')[1];
            if(classSplit == 'right') {
                $(this).children().last().css('margin-left', '30px');
            } else {
                $(this).children().last().css('margin-left', '80px');
            }
        });
    });

    $('#select2').on('change', function() {
        var userID = $(this).val();
        getAllConversation(userID);
        $(".select2.select2-container.select2-container--default").hide("slow");
        $('#searchNewUserForNewMessage').show(1000);
    });

    $('#searchNewUserForNewMessage').on('click', function() {
        $('#searchNewUserForNewMessage').hide("fast");
        $(".select2.select2-container.select2-container--default").show(1000);
    });

    $('#sendMessage').keydown(function(e) {
        var key         = e.which;
        var receiverID  = $("input[name='receiverID']").val();

        if (key == 13) {
            var message     = $('#messageTextarea').val();
            if(e.shiftKey) {
                $('#messageTextarea').val(message+"\n");
            } else {
                $.ajax({
                    method: 'POST',
                    url: '{{ route('send.message') }}',
                    data: {reciverID : receiverID, message: message},
                    datatype: 'html',
                    success: function (data) {
                        console.log(data);
                        $('#conversation').html(data['messages']);
                        //                        $('#conversation').css({'overflow-y:hidden;'});
                        $('#userInbox').html(data['inbox']);
                        deleteConversation(data['conversationID']);
                        slimScrollReInitiate();
                    }
                });
                $('#messageTextarea').val('');

            }

            return false;
        }
    });

    function loadConversationOnClick(userID) {
        $("select").val(userID).trigger("change");
    }

    function deleteConversation(convID) {
        var route  = "{{ route("delete.conversation", null) }}";
        var string = '<a href="'+ route +'/'+ convID +'">{{ trans('Message::message.delete_success_two') }}</a>';
        $('#deleteConversation').html(string);
    }

    function printChatWithUserName(username, userid) {
        if(userid != 0) {
            $('#chatWith').removeClass('hidden');
            var route  = "";
            var string = '<a href="'+ route +'/'+ userid +'">'+username+'</a>';
            $('.chatUserName').html(string);
        } else {
            $('#chatWith').addClass('hidden');
            $('.chatUserName').html("");
        }
    }

    function getAllConversation(userID) {
        $("input[name='receiverID']").val(userID);
        if(userID != 0) {
            $.ajax({
                method: 'POST',
                url: '{{ route('read.message') }}',
                data: {userID : userID},
                datatype: 'html',
                success: function (data) {
                    $('#conversation').html(data['messages']);
                    printChatWithUserName(data['userFullName'], userID);
                    deleteConversation(data['conversationID']);
                    slimScrollReInitiate();

                }
            });
        } else {
            printChatWithUserName(null, userID);
        }

    }

    function slimScrollReInitiate() {
        $(".slimScrollBar").remove();
        $(".slimScrollRail").remove();
        $(".slimScrollDiv").contents().unwrap();
        $('.direct-chat-messages').slimScroll({
            height: '400x',
            start: 'bottom'
        });
        $('.message-user').slimScroll({
            height: '485px'
        });
    }
</script>

<style type="text/css">
    .select2-drop {
        display: none !important;
    }
    .select2-container--default .select2-selection--single {
        border-radius: 0px !important;
        margin: -3px 2px 0px 2px !important;
        height: 30px !important;
        border: 1px solid #c2cad8 !important;
        padding: 4px 12px !important;
    }
</style>
