<div class="col-sm-8 set-padding">
    <div class="box-message-topbar">
        
        <div class="input-group lihight">
            <h6 class="inline {{ @$last_chat_user->id == 0 ? 'hidden' : '' }}" id="chatWith">
                {{ trans("Message::message.chat") }}
                <small>{{ trans("Message::message.with") }}</small>
            </h6>
            <h6 class="inline chatUserName">
                {{--{{ dd(@$last_chat_user->id == 0) }}--}}
                @if(@$last_chat_user->id != 0)
                    <a href="{{ url('user/view/'. @$last_chat_user->id) }}">{{ @$last_chat_user->name == null || @$last_chat_user->name == ' ' ? @$last_chat_user->username : @$last_chat_user->name}}</a>
                @endif
            </h6>
            <div class="btn-group pull-right lihight-btn">
                <button id="searchNewUserForNewMessage" class="btn btn-primary btn-sm" type="button">{{ trans("New Message") }}</button>

                <select class="btn btn-sm " name="user" id="select2">
                    <option value="0">Select User</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}" {{ @$last_chat_user->id == $user->id ? 'selected' : '' }}>{{ $user->name == null || $user->name == ' ' ? $user->username : $user->name  }}</option>
                    @endforeach
                </select>


                <div class="btn-group pull-right">
                    <button data-toggle="dropdown" class="btn btn-default btn-sm" type="button" aria-expanded="false">
                        <i class="fa fa-cog"></i>
                    </button>
                    <ul class="dropdown-menu bg-gray">
                        <li id="deleteConversation"><a href="{{ route('delete.message', $last_conversation_id) }}">{{ trans("Message::message.buttons.actions.delete") }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('message::user.message.conversation')

    <div class="box-footer box-message-footer ">

        {!!Form::open()->action(route('send.message'))->class('form-horizontal')->id('sendMessage') !!}

        <input type="hidden" value="{{ @$last_chat_user->id }}" name="receiverID">
            <div class="direct-chat-msg">

                <!-- /.direct-chat-info -->
                <div class="direct-chat-text">
                    <textarea type="text" class="form-control message-comment-box" placeholder="{{ trans("Message::message.placeholder") }}....." name="message" id="messageTextarea"></textarea>
                </div>
                <!-- /.direct-chat-text -->
            </div>

        {!! Form::close() !!}
    </div>

</div>