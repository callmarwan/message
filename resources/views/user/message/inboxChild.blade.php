<ul class="message-user hidden-xs " id="userInbox">
    {{--{{dd($inbox)}}--}}
    @foreach($inbox as $msg)

        <li>
            <a href="javascript:void(0);" onclick="loadConversationOnClick({{$msg->withUser->id}}); deleteConversation({{ @$msg->thread->conversation_id }});">
                <div class="pull-left">
                    <img src="{{ url($msg->withUser->photo ? $msg->withUser->photo : "" )  }}" class="img-circle" alt="User Image">
                </div>
                <h4>
                    <span>{{ $msg->withUser->name }}</span>
                    <small class="pull-right"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse(@$msg->thread->created_at)->diffForHumans() }}</small>
                </h4>
                <p>{{ @$msg->thread->message}}</p>

            </a>
        </li>
    @endforeach
</ul>