This is a Laravel 5 package that provides message management facility for souktel framework.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `souktel/message`.

    "souktel/message": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

```php
SoukTel\Message\Providers\MessageServiceProvider::class,

```

And also add it to alias

```php
'Message'  => SoukTel\Message\Facades\Message::class,
```

Use the below commands for publishing

Migration and seeds

    php artisan vendor:publish --provider="SoukTel\Message\Providers\MessageServiceProvider" --tag="migrations"
    php artisan vendor:publish --provider="SoukTel\Message\Providers\MessageServiceProvider" --tag="seeds"

Configuration

    php artisan vendor:publish --provider="SoukTel\Message\Providers\MessageServiceProvider" --tag="config"

Language

    php artisan vendor:publish --provider="SoukTel\Message\Providers\MessageServiceProvider" --tag="lang"

Views public and admin

    php artisan vendor:publish --provider="SoukTel\Message\Providers\MessageServiceProvider" --tag="view-public"
    php artisan vendor:publish --provider="SoukTel\Message\Providers\MessageServiceProvider" --tag="view-admin"

Publish admin views only if it is necessary.

## Usage


