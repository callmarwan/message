<?php

// Admin web routes  for message
Route::group(['prefix' => trans_setlocale() . '/admin/message'], function () {
    Route::get('/message/sub/{slug}/{id}', 'SoukTel\Message\Http\Controllers\MessageAdminController@updateSubStatus');
    Route::get('/message/status/{slug}', 'SoukTel\Message\Http\Controllers\MessageAdminController@updateStatus');
    // Route::get('/message/Inbox', 'MessageAdminController@inbox');
    // Route::get('/compose', 'MessageAdminController@compose');
    Route::get('/search/{slug?}/{status?}', 'SoukTel\Message\Http\Controllers\MessageAdminController@search');
    Route::get('/status/{status?}', 'SoukTel\Message\Http\Controllers\MessageAdminController@showMessage');
    Route::get('/details/{caption}/{slug}', 'SoukTel\Message\Http\Controllers\MessageAdminController@getDetails');
    Route::get('/reply/{id}', 'SoukTel\Message\Http\Controllers\MessageAdminController@reply');
    Route::get('/forward/{id}', 'SoukTel\Message\Http\Controllers\MessageAdminController@forward');
    Route::resource('/message', 'SoukTel\Message\Http\Controllers\MessageAdminController');
    Route::get('/important/substatus', 'SoukTel\Message\Http\Controllers\MessageAdminController@changeSubStatus');
    Route::get('/starred', 'SoukTel\Message\Http\Controllers\MessageAdminController@starredMessages');
});

// Admin API routes  for message
Route::group(['prefix' => trans_setlocale() . 'api/v1/admin/message'], function () {
    Route::resource('message', 'SoukTel\Message\Http\Controllers\MessageAdminApiController');
});

// User web routes for message
Route::group(['prefix' => trans_setlocale() . '/user/message'], function () {

    Route::resource('/message', 'SoukTel\Message\Http\Controllers\MessageUserController');

    Route::get('inbox', 'SoukTel\Message\Http\Controllers\MessageUserController@index')->name('inbox.message');
    Route::get('get-user-inbox', 'SoukTel\Message\Http\Controllers\MessageUserController@getUserInbox')->name('inbox.get_user_inbox');
    Route::get('get-latest-conversations', 'SoukTel\Message\Http\Controllers\MessageUserController@getLatestConversations')->name('get_latest_conversations');
    Route::post('read', 'SoukTel\Message\Http\Controllers\MessageUserController@readMessage')->name('read.message');
    Route::delete('delete/{id}', 'SoukTel\Message\Http\Controllers\MessageUserController@deleteMessage')->name('delete.message');
    Route::post('send', 'SoukTel\Message\Http\Controllers\MessageUserController@send')->name('send.message');
    Route::delete('conversation/delete/{id}', 'SoukTel\Message\Http\Controllers\MessageUserController@deleteConversation')->name('delete.conversation');
    Route::post('conversation/block/{id}', 'SoukTel\Message\Http\Controllers\MessageUserController@blockConversation')->name('block.conversation');

});

// User API routes for message
Route::group(['prefix' => trans_setlocale() . 'api/v1/user/message'], function () {
    Route::resource('message', 'SoukTel\Message\Http\Controllers\MessageUserApiController');
});

// Public web routes for message
Route::group(['prefix' => trans_setlocale() . '/messages'], function () {
    Route::get('/', 'SoukTel\Message\Http\Controllers\MessageController@index');
    Route::get('/{slug?}', 'SoukTel\Message\Http\Controllers\MessageController@show');
});

// Public API routes for message
Route::group(['prefix' => trans_setlocale() . 'api/v1/messages'], function () {
    Route::get('/', 'SoukTel\Message\Http\Controllers\MessagePublicApiController@index');
    Route::get('/{slug?}', 'SoukTel\Message\Http\Controllers\MessagePublicApiController@show');
});
