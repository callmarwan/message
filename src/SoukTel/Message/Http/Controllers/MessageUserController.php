<?php

namespace SoukTel\Message\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

use Form;
use SoukTel\Message\Http\Requests\MessageUserRequest;
use SoukTel\Message\Interfaces\MessageRepositoryInterface;
use App\User;
use Nahid\Talk\Facades\Talk;
use SoukTel\Message\Models\Message;

class MessageUserController extends BaseController
{
    /**
     * Initialize message controller.
     *
     * @param type MessageRepositoryInterface $message
     *
     * @return type
     */
    protected $authUser;

    public function __construct(MessageRepositoryInterface $message)
    {
        $this->repository = $message;
        $this->middleware('web');
        $this->middleware('auth:web');
        $this->middleware('auth.active:web');
        $this->setupTheme(config('theme.themes.user.theme'), config('theme.themes.user.layout'));

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(MessageUserRequest $request)
    {
        $userId = 0;
        if ($request->get('with')) {
            $userId = $request->get('with');
        }
        Talk::setAuthUserId(user_id());

        $this->theme->asset()->add('chat-css', 'vendor/chat/chat.css');

        $this->theme->asset()->add('select2-css', 'packages/select2/css/select2.min.css');
        $this->theme->asset()->container('footer')->add('select2-js', 'packages/select2/js/select2.full.js');

        $this->theme->prependTitle(trans('message::message.names'));


        $inbox = Talk::threads();

        $users = User::join('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('id', '<>', user('web')->id)
            ->whereStatus('Active')
            ->whereIn('role_user.role_id', user('web')->getRoles->pluck('id')->toArray())
            ->get();

        $last_chat_user = [];

        $conversationID = 0;

        if ($userId) {
            $conversations = Talk::getMessagesByUserId($userId);

            if (!$conversations) {
                $last_chat_user = User::find($userId);
            } else {
                $conversationID = 0;

                foreach ($inbox as $i) {
                    if ($i->withUser->id == $userId) {
                        $conversationID = $i->thread->conversation_id;
                        break;
                    }
                }
                $last_chat_user = $conversations->withUser;

                $messages = Talk::getConversationMessages($conversationID);
            }
        } else {
            $conversation = (!empty($inbox) ? @Talk::getMessagesByUserId($inbox[0]->withUser->id) : []);
            if ($conversation) {
                $last_chat_user = $conversation->withUser;

                $messages = Talk::getConversationMessages($inbox[0]->thread->conversation_id);
            }
        }

        $last_conversation_id = $conversationID ?: (!empty($inbox) ? @$inbox[0]->thread->conversation_id : 0);

        $inbox = !empty($inbox) ? $inbox : [];
        return $this->theme->of('message::user.message.index', compact('users', 'messages', 'last_chat_user', 'last_conversation_id', 'inbox', 'userId'))->render();
    }

    public function readMessage(MessageUserRequest $request)
    {
        return $this->messageInfo($request['userID'], $request['offset'], $request['take']);
    }

    public function send(MessageUserRequest $request)
    {
        Talk::setAuthUserId(user_id());

        $reciverID = $request['reciverID'];
        $message = $request['message'];
        if (empty($reciverID) || empty($message)) {
            return '';
        }
        $send = Talk::sendMessageByUserId($reciverID, $message);

        if ($send) {
            return $this->messageInfo($reciverID);
        } else {
            return ['block_message' => trans('message.block_message')];
        }
    }

    protected function messageInfo($userID, $offset = 0, $take = 20)
    {
        Talk::setAuthUserId(user_id());

        $ajaxView = [];
        $inbox = Talk::threads();
        $conversations = Talk::getMessagesByUserId($userID);

        $messages = [];
        $conversationID = 0;

        if (!$conversations) {
            $user = User::find($userID);
        } else {
            $conversationID = 0;

            foreach ($inbox as $i) {
                if ($i->withUser->id == $userID) {
                    $conversationID = $i->thread->conversation_id;
                    break;
                }
            }
            $user = $conversations->withUser;

            $messages = Talk::getConversationMessages($conversationID, $offset, $take);
        }


        $ajaxView['messages'] = view("message::user.message.conversation", compact('messages'))->render();
        $ajaxView['chat'] = view("message::user.message.partial.chat", compact('messages'))->render();
        $ajaxView['inbox'] = view("message::user.message.inboxChild")->withInbox($inbox)->render();
        $ajaxView['userFullName'] = str_limit(($user->nickname ? $user->nickname : $user->name), 50);
        $ajaxView['conversationID'] = $conversationID;
        return $ajaxView;
    }

    public function getUserInbox()
    {
        Talk::setAuthUserId(user_id());

        $inbox = Talk::threads();

        $ajaxView['inbox'] = view("message::user.message.inboxChild")->with(compact('inbox'))->render();

        return $ajaxView;
    }

    public function getLatestConversations()
    {
        Talk::setAuthUserId(user_id());
        $inbox = Talk::threads($order = 'desc', $offset = 0, $take = 3);

        $unread_messages_count = Message::join('conversations', 'conversations.id', '=', 'messages.conversation_id')
            ->where(function ($query) {
                $query->where('conversations.user_one', user_id())->orWhere('conversations.user_two', user_id());
            })
            ->where('messages.is_seen', 0)->where('messages.user_id', '!=', user_id())->count();

        $ajaxView['messages'] = view('vendor.message.user.message.partial.messages')->with(compact('inbox', 'unread_messages_count'))->render();

        return $ajaxView;
    }

    public function deleteMessage(MessageUserRequest $request, $id)
    {
        Talk::setAuthUserId(user_id());
        $message_id = $id;
        try {
            if ($request->ajax()) {

                if (Talk::deleteMessage($message_id)) {
                    return response()->json([
                        'message' => trans('messages.success.delete', ['Module' => trans('message.message')]),
                        'code' => 204,
                        'status' => "success"
                    ])->setStatusCode(201);
                }
            }
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4004,
            ])->setStatusCode(400);
        }
    }

    public function deleteConversation($id)
    {
        Talk::setAuthUserId(user_id());

        try {
            if (!empty(Talk::getConversationsById($id))) {
                Talk::deleteConversations($id);
            }
            return response()->json([
                'message' => trans('messages.success.deleted', ['Module' => trans('message.conversation')])
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4004,
            ])->setStatusCode(400);
        }
    }

    public function blockConversation($id)
    {
        Talk::setAuthUserId(user_id());

        try {
            if (!empty(Talk::getConversationsById($id))) {
                Talk::blockConversations($id);
                return $this->getUserInbox();
            }
            return response()->json([
                'message' => trans('messages.success.update', ['Module' => trans('message.conversation')])
            ], 201);

        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code' => 4004,
            ])->setStatusCode(400);
        }
    }


}
